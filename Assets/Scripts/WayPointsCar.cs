using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPointsCar : MonoBehaviour
{
   
    public bool destruirPoint;
    public bool adelante;
    public GameObject points;
    public int indiceDePosicion = 0;
    public int indiceControl=1;
    public float pruebaInstanciador=0;
    public Transform origenPoint;
    bool instanciar;
    void Start()
    {
        Instantiate(points, origenPoint.position, transform.rotation);
    }

    
    void Update()
    {
        //Movimiento();


        //if(pruebaInstanciador >0.05)
        //{
        //    instanciar = true;
        //}
        //else
        //{
            
        //    instanciar = false;
        //}
        CrearWayPoints();

        //StartCoroutine(CrearPoints(instanciar));
        
        
    }


    void Movimiento()
    {
        if(Input.GetKey(KeyCode.W))
        {
            
            pruebaInstanciador += 0.1f * Time.deltaTime;
           
        }
        else
        {
            pruebaInstanciador =0.0f;
        }

       
       
    }


    void CrearWayPoints()
    {
        if ( destruirPoint)
        {
            for (int i = 0; i < indiceControl; i++)
            {
                Instantiate(points, origenPoint.position, transform.rotation);
                pruebaInstanciador = 0.0f;
                destruirPoint = false;
            }
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("point"))
        {
            destruirPoint = true;
            Destroy(other.gameObject);
            //indiceControl += 1;
            indiceDePosicion += 1;
        }
    }



    
}

