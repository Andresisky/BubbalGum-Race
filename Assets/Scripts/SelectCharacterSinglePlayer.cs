using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectCharacterSinglePlayer : MonoBehaviour
{
    public GameObject arrows1;
    public GameObject[] playerCar1;
    public Transform targetP1;
    public Transform targetP3;
    public GameObject powerUp;
    public GameObject gas;
    public GameObject[] c1;
    public GameObject cars1;
    public GameObject trafficLights;
    public Image p1Ready;
    public Image p1;
    public Sprite p3;
    //public GameObject posiciones;
    public bool seleccionLista1;
    public int indiceDeSeleccion = 0;
    bool primerJugador;
    public bool del2;
    public bool red1;
    void Start()
    {
        PlayerPrefs.DeleteAll();
        primerJugador = false;
 
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            primerJugador = true;
        }

        


        if (primerJugador)
        {
            p1.sprite = p3;
            arrows1.gameObject.SetActive(true);
            cars1.SetActive(true);

            if (Input.GetKeyDown(KeyCode.Q))
            {
                p1Ready.gameObject.SetActive(true);
                seleccionLista1 = true;
            }

        }

        

        if (indiceDeSeleccion == 0)
        {
            c1[0].gameObject.SetActive(true);
            c1[1].gameObject.SetActive(false);
            c1[2].gameObject.SetActive(false);
        }
        if (indiceDeSeleccion == 1)
        {
            red1 = true;
            c1[0].gameObject.SetActive(false);
            c1[1].gameObject.SetActive(true);
            c1[2].gameObject.SetActive(false);
        }
        if (indiceDeSeleccion == 2)
        {
            c1[0].gameObject.SetActive(false);
            c1[1].gameObject.SetActive(false);
            c1[2].gameObject.SetActive(true);
        }


        if (seleccionLista1)
        {
            powerUp.SetActive(true);
            gas.SetActive(true);
            //posiciones.SetActive(true);
            Destroy(this.gameObject);
            Instantiate(playerCar1[indiceDeSeleccion], targetP1.position, targetP1.rotation);
            Instantiate(trafficLights, targetP3.position, targetP3.rotation);

        }
    }



    public void SelectComplete()
    {
        if (seleccionLista1)
        {
            SceneManager.LoadScene(2);
        }

    }


    public void SelectCar()
    {
        if (seleccionLista1 == false)
        {
            if (indiceDeSeleccion < c1.Length - 1)
            {
                indiceDeSeleccion += 1;

            }
        }




    }

    public void SelectCarNegativo()
    {
        if (seleccionLista1 == false)
        {
            if (indiceDeSeleccion > -1)
            {
                indiceDeSeleccion -= 1;

            }
        }



    }

}
