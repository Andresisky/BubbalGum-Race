using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelect : MonoBehaviour
{
    public GameObject[] button;
    public int indiceSelec;
    void Start()
    {
        indiceSelec = 0;
    }

   
    void Update()
    {
        if (indiceSelec == 0)
        {
            button[0].SetActive(true);
            button[1].SetActive(false);
        }

        if (indiceSelec == 1)
        {
            button[0].SetActive(false);
            button[1].SetActive(true);
        }
    }

    public void Positivo()
    {
        if(indiceSelec<button.Length-1)
        {
            indiceSelec += 1;
        }

       


    }

    public void Negativo()
    {
        if(indiceSelec>-1)
        {
            indiceSelec -= 1;
        }
        
    }

    public void Select()
    {
        SceneManager.LoadScene(2);
    }
}
