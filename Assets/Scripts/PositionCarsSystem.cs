using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PositionCarsSystem : MonoBehaviour
{
    public Text pos1;
    public Text pos2;
    public Text pos3;

    public WayPointsCar car1;
    public WayPointsCar car2;
    public WayPointsCar car3;
    void Start()
    {
        
    }

    
    void Update()
    {
        Primeros();
        Segundos();
        Terceros();
    }


    void Primeros()
    {
        if (car1.indiceDePosicion > car2.indiceDePosicion && car1.indiceDePosicion > car3.indiceDePosicion)
        {
            pos1.text = "Delorean Primer Lugar";
        }

        if (car2.indiceDePosicion > car1.indiceDePosicion && car2.indiceDePosicion > car3.indiceDePosicion)
        {
            pos1.text = "Mach5 Primer Lugar";
        }

        if (car3.indiceDePosicion > car1.indiceDePosicion && car3.indiceDePosicion > car2.indiceDePosicion)
        {
            pos1.text = "RedKiller Primer Lugar";
        }
    }

    void Segundos()
    {
        if (car1.indiceDePosicion > car2.indiceDePosicion && car1.indiceDePosicion < car3.indiceDePosicion)
        {
            pos2.text = "Delorean Segundo Lugar";
        }

        if (car1.indiceDePosicion > car3.indiceDePosicion && car1.indiceDePosicion < car2.indiceDePosicion)
        {
            pos2.text = "Delorean Segundo Lugar";
        }

        if (car2.indiceDePosicion > car1.indiceDePosicion && car2.indiceDePosicion < car3.indiceDePosicion)
        {
            pos2.text = "Mach5 Segundo Lugar";
        }

        if (car2.indiceDePosicion < car1.indiceDePosicion && car2.indiceDePosicion > car3.indiceDePosicion)
        {
            pos2.text = "Mach5 Segundo Lugar";
        }

        if (car3.indiceDePosicion > car1.indiceDePosicion && car3.indiceDePosicion < car2.indiceDePosicion)
        {
            pos2.text = "RedKiller Segundo Lugar";
        }

        if (car3.indiceDePosicion < car1.indiceDePosicion && car3.indiceDePosicion > car2.indiceDePosicion)
        {
            pos2.text = "RedKiller Segundo Lugar";
        }
    }


    void Terceros()
    {
        if (car1.indiceDePosicion < car2.indiceDePosicion && car1.indiceDePosicion < car3.indiceDePosicion)
        {
            pos3.text = "Delorean Tercer Lugar";
        }

       

        if (car2.indiceDePosicion < car1.indiceDePosicion && car2.indiceDePosicion < car3.indiceDePosicion)
        {
            pos3.text = "Mach5 Tercer Lugar";
        }

        

        if (car3.indiceDePosicion < car1.indiceDePosicion && car3.indiceDePosicion < car2.indiceDePosicion)
        {
            pos3.text = "RedKiller Tercer Lugar";
        }

    }
}
