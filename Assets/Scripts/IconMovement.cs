using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconMovement : MonoBehaviour
{
    float velocidad = 120;
    Transform box;
    Rigidbody rb;
    Collider col;

    void Start()
    {
        box = GetComponent<Transform>();
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        StartCoroutine(Spawn());
    }

    
    void Update()
    {
        box.transform.Rotate(velocidad * Time.deltaTime, 0, velocidad * Time.deltaTime);
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(3f);
        rb.isKinematic = true;
        col.isTrigger = true;
    }

}
