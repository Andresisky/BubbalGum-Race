using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaExtraScript : MonoBehaviour
{
    public Image barra;
    public float vida = 100f;
    public bool extra;
    public GameObject effect;
    void Start()
    {
        
    }

    
    void Update()
    {
       



        if(Input.GetKeyDown(KeyCode.T))
        {
            extra = true;
        }
        Activo();
    }


    void Activo()
    {
        if(extra)
        {
            effect.SetActive(true);
            vida += 0.1f;
        }

        if(vida>=100f)
        {
            extra = false;
            effect.SetActive(false);
        }
       
        
    }
}
