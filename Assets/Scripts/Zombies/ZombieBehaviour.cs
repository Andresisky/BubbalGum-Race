using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class ZombieBehaviour : MonoBehaviour
{

    //Basic Variables
    public bool ragdoll = false;
    public CapsuleCollider mainCollider;
    public GameObject rig;


    //Car Variables
    [SerializeField] ArcadeCar[] carScript;
    public float carSpeed;
    public float[] carsSpeed;

    private NavMeshAgent agent;
    GameObject car;
    [SerializeField] GameObject[] cars;

    //Animator
    Animator anim;
    bool screaming = false;
    public bool attacking = false;
    public GameObject bloodParticles;


    float countdown = 7;
    float stopwatch = 0;

    void Start()
    {
        cars = GameObject.FindGameObjectsWithTag("Player");
        GetComponent<Rigidbody>().isKinematic = false;
        carScript = GameObject.FindObjectsOfType<ArcadeCar>();
        anim = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
        car = cars[Random.Range(0, cars.Length)];
        //car = GameObject.FindWithTag("Player"); ------------------------
        screaming = true;
        GetRagdollBits();
        RagdollModeOFF();
    }

    void Update()
    {
        //car = GameObject.FindWithTag("Player");
        //carSpeed = carScript.GetSpeed();
        for (int i = 0; i < carScript.Length; i++)
        {
            carsSpeed[i] = carScript[i].GetSpeed();
        }

        if (ragdoll == true)
        {
            RagdollModeON();
            stopwatch += Time.deltaTime;
            if (stopwatch >= countdown)
            {
                Instantiate(bloodParticles, ragdollColliders[1].transform.position, transform.rotation);
                Destroy(this.gameObject);
            }
        }
        else
        {
            stopwatch = 0;
            RagdollModeOFF();
            agent.SetDestination(car.transform.position);
        }

        if (screaming == true)
        {
            agent.isStopped = true;
            StartCoroutine(WaitScream());
        }

        if (attacking == true)
        {
            agent.isStopped = true;
        }

    }


    void Attacking()
    {
        attacking = true;
        GetComponent<Rigidbody>().isKinematic = true;
        anim.SetTrigger("isAttacking");
        agent.isStopped = true;
        StartCoroutine(WaitAttack());
    }
    IEnumerator WaitScream()
    {
        yield return new WaitForSeconds(2);
        agent.isStopped = false;
        screaming = false;
    }


    IEnumerator WaitAttack()
    {
        yield return new WaitForSeconds(2);
        GetComponent<Rigidbody>().isKinematic = false;
        attacking = false;
    }



    Collider[] ragdollColliders;
    Rigidbody[] limbsRb;
    void GetRagdollBits()
    {
        ragdollColliders = rig.GetComponentsInChildren<Collider>();
        limbsRb = rig.GetComponentsInChildren<Rigidbody>();
    }

    void RagdollModeON()
    {
        anim.enabled = false;
        foreach (Collider col in ragdollColliders)
        {
            col.enabled = true;
        }

        foreach (Rigidbody rigid in limbsRb)
        {
            rigid.isKinematic = false;
        }

        mainCollider.enabled = false;
        GetComponent<Rigidbody>().isKinematic = false;
        agent.isStopped = true;
    }

    void RagdollModeOFF()
    {
        foreach(Collider col in ragdollColliders)
        {
            col.enabled = false;
        }

        foreach(Rigidbody rigid in limbsRb)
        {
            rigid.isKinematic = true;  
        }

        agent.enabled = true;
        anim.enabled = true;
        mainCollider.enabled = true;
        //GetComponent<Rigidbody>().isKinematic = false;
        agent.isStopped = false;
    }




    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            for (int i = 0; i < cars.Length; i++)
            {
                if (carsSpeed[i] < 10f)
                {
                    Attacking();
                }
                if (carsSpeed[i] > 10f && carSpeed < 50f)
                {
                    Instantiate(bloodParticles, transform.position, transform.rotation);
                    ragdoll = true;
                }

                if (carsSpeed[i] > 50f)
                {
                    Instantiate(bloodParticles, transform.position, transform.rotation);
                    Destroy(this.gameObject);
                }
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Laser_PU")
        {
            Destroy(this.gameObject);
        }
        if (other.gameObject.tag == "Hook_PU")
        {
            Instantiate(bloodParticles, transform.position, transform.rotation);
            Destroy(this.gameObject);
        }
    }


}
