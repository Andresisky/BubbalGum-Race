using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DamageCar : MonoBehaviour
{
    public Image lifebar;


    [SerializeField]
    float cantidad = 100f;

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        cantidad = Mathf.Clamp(cantidad, 0, 100);
        lifebar.fillAmount = cantidad / 100;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {

            cantidad -=0.1f;
            Debug.Log("ij");
            Destroy(other.gameObject);
        }
    }
}
