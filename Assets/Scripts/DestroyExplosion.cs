using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyExplosion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("DesExp");
    }

    IEnumerator DesExp()
    {
        yield return new WaitForSeconds(3f);
        Destroy(this.gameObject);
    }
}
