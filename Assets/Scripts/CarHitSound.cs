using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarHitSound : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip[] carHits;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = carHits[Random.Range(0, carHits.Length)];
        audioSource.PlayOneShot(audioSource.clip);
        Destroy(this.gameObject, 2f);
    }
}
