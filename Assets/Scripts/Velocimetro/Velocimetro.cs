using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Velocimetro : MonoBehaviour
{
    public ArcadeCar RR;
    public GameObject neeple;
    private float starPosition = 0f, endPosition = -210f;
    private float desiredPosition;

    public float vehiculeSpeed;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        vehiculeSpeed = RR.speed;
        updateNeedle();
    }
    public void updateNeedle()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            endPosition = 210;
        }
        else
        {
            endPosition = -210;
        }
        desiredPosition = starPosition - endPosition;
        float temp = vehiculeSpeed / 50;
        neeple.transform.eulerAngles = new Vector3(0, 0, (starPosition - temp * desiredPosition));
    }
}
