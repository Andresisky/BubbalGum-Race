using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class agua : MonoBehaviour
{
    public GameObject particulas;
    public void Start()
    {
        particulas.SetActive(false);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("water"))
        {
            particulas.SetActive(true);
        }
        else
        {
            particulas.SetActive(false);
        }
    }
}
