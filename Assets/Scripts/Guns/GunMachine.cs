using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.VFX;
using UnityEngine.InputSystem;

public class GunMachine : MonoBehaviour
{
    public GameObject bullet;
    public Transform[] spawnPoint;

    public float shotForce = 3500;
    public float shotRace = 0.1f;
    public VisualEffect effectFlash;
    public VisualEffect effectFlash2;
    private float shotRaceTime = 0;

    public bool activeGun = false;

    public Text bulletContTXT;
    public Text cooldownTimeTXT;
    public Text bulletTopTXT;
    public int bulletCont;
    public float cooldownTimer;
    //private string TiempoPrefs = "tiempo";

    private float auxTimer;
    private int auxBullets;



    private bool shoot = false;
    public void ShootGun(InputAction.CallbackContext context)
    {
        shoot = context.action.triggered;
    }

    void Start()
    {
        auxTimer = cooldownTimer;
        auxBullets = bulletCont;
        bulletTopTXT.text = auxBullets.ToString();
        bulletContTXT.text = bulletCont.ToString();
        //cooldownTimeTXT.text = cooldownTimer.ToString();
        cooldownTimeTXT.text = " ";
    }

    void Update()
    {
        activeGun = false;
        //disparo mouse  
        if (shoot)
        {
            activeGun = true;
            if (activeGun == true)
            {
                Shoot();
            }
        }

        //Contador de Recarga
        if (bulletCont <= 0)
        {
            cooldownTimer -= Time.deltaTime;
            cooldownTimeTXT.text = cooldownTimer.ToString();
        }
        if (cooldownTimer <= 0)
        {
            ReloadGun();
        }
        /*if (Input.GetKeyDown(KeyCode.R))
        {

            // rechargeTimer -= Time.deltaTime;
        }*/
    }

    public void Shoot()
    {
        GameObject nextBullet;
        if (bulletCont > 0)
        {
            if (Time.time > shotRaceTime)
            {
                for (int i = 0; i < spawnPoint.Length; i++)
                {
                    SoundSystem.instance.PlayShoot();
                    nextBullet = Instantiate(bullet, spawnPoint[i].position, spawnPoint[i].rotation);
                    //nextBullet = PhotonNetwork.Instantiate("Bullet", spawnPoint[i].position, spawnPoint[i].rotation);
                    effectFlash.Play();
                    effectFlash2.Play();

                    //Force Bullet
                    nextBullet.GetComponent<Rigidbody>().AddForce(spawnPoint[i].forward * shotForce);

                    shotRaceTime = Time.time + shotRace;
                    Destroy(nextBullet, 2);
                }
                bulletCont -= spawnPoint.Length;
            }
            bulletContTXT.text = bulletCont.ToString();
        }
    }

    public void ReloadGun()
    {
        cooldownTimer = auxTimer;
        bulletCont = auxBullets;
        cooldownTimeTXT.text = cooldownTimer.ToString();
        cooldownTimeTXT.text = " ";
        bulletContTXT.text = bulletCont.ToString();
        //Debug.Log("Reloaded");
    }

}
