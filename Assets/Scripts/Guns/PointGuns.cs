using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PointGuns : MonoBehaviour
{

    public GameObject[] lasers;
    [SerializeField] bool gunActive = false;
    public Transform[] gunsX;
    public Transform[] gunsY;
    //[Range(0,50)]
    float aimSpeed = 125f;

    float gunsAngleX;
    float gunsAngleY;
    Quaternion idlePointX;
    Quaternion idlePointY;


    private bool pointGun = false;
    public void PointGun(InputAction.CallbackContext context)
    {
        pointGun = context.action.triggered;
    }

    void Start()
    {
        for (int i = 0; i < gunsX.Length; i++)
        {
            Quaternion idlePointX = gunsX[i].rotation;
            Quaternion idlePointY = gunsY[i].rotation;
        }
        for (int i = 0; i < lasers.Length; i++)
        {
            lasers[i].SetActive(false);
        }
    }

    void Update()
    {
        gunActive = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        for (int i = 0; i < lasers.Length; i++)
        {
            lasers[i].SetActive(false);
        }


        if (pointGun)
        {
            for (int i = 0; i < lasers.Length; i++)
            {
                lasers[i].SetActive(true);
            }
            gunActive = true;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Aim();
        }


        if (pointGun)
        {
            StartCoroutine(CursorToCenter());
        }
    }

    void RotateGunsX()
    {
        gunsAngleX += Input.GetAxis("Mouse X") * aimSpeed * Time.deltaTime;
        gunsAngleX = Mathf.Clamp(gunsAngleX, -30, 30);
        for (int i = 0; i < gunsX.Length; i++)
        {
            gunsX[i].localRotation = Quaternion.AngleAxis(gunsAngleX, Vector3.up);
        }
    }

    void RotateGunsY()
    {
        gunsAngleY += Input.GetAxis("Mouse Y") * aimSpeed * -Time.deltaTime;
        gunsAngleY = Mathf.Clamp(gunsAngleY, -20, 10);
        for (int i = 0; i < gunsY.Length; i++)
        {
            gunsY[i].localRotation = Quaternion.AngleAxis(gunsAngleY, Vector3.right);
        }
    }


    void Aim()
    {
        RotateGunsX();
        RotateGunsY();
    }

    IEnumerator CursorToCenter()
    {
        gunsAngleX = 0;
        gunsAngleY = 0;
        Cursor.lockState = CursorLockMode.Locked;
        yield return new WaitForSeconds(0.01f);
        for (int i = 0; i < gunsX.Length; i++)
        {
            float idleTime = 0.5f;
            gunsX[i].rotation = idlePointX;
            gunsY[i].rotation = idlePointY;
        }
    }
}
