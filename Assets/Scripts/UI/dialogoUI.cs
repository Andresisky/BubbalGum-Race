using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dialogoUI : MonoBehaviour
{
    public int RanTimerHigh = 2;
    public int RanTimerLow = 1;
    public int RandomInt;
    void Start()
    {
         RandomInt = Random.Range(RanTimerLow, RanTimerHigh);
    }
    private void Update()
    {
        RandomInt = Random.Range(RanTimerLow, RanTimerHigh);
    }
    public void DialogoDelorean()
    {
        if (RandomInt == 1)
        {
            SoundSystem.instance.PlayseleD();
        }
        if (RandomInt == 3)
        {
            SoundSystem.instance.PlayseleD2();
        }


    }
    public void DialogoMach()
    {
        if (RandomInt == 1)
        {
            SoundSystem.instance.PlayseleM();
        }
        if (RandomInt == 3)
        {
            SoundSystem.instance.PlayseleM2();
        }

    }
    public void DialogoBati()
    {
     
        if (RandomInt == 1)
        {
            SoundSystem.instance.PlayseleB();
        }
        if (RandomInt == 3)
        {
            SoundSystem.instance.PlayseleB2();
        }
    }


}
