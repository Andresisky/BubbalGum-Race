using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public void Load(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }


    public void OnJoinedRoom()
    {
        SceneManager.LoadScene("Lobby");
    }

    public void doExitGame()
    {
        Application.Quit();
    }

}
