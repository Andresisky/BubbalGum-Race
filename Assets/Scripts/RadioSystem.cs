using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;

public class RadioSystem : MonoBehaviour
{

   
    public int indice =0;
    public Image icon;
    bool off = false;
    int encenderIndice;
    
    public Text audioText;
    public Text emisoraText;
    public AudioClip cancion1;
    public AudioClip cancion2;
    public AudioClip cancion3;
    public AudioClip cancion4;
    public AudioClip cancion5;
   
    public AudioSource estacion1;
    public AudioSource estacion2;
    public AudioSource estacion3;

    private bool radioOn = false;
    [SerializeField] public Vector2 scroll = Vector2.zero;
    public void RadionOn(InputAction.CallbackContext context)
    {
        radioOn = context.action.triggered;
    }

    public void Scroll(InputAction.CallbackContext context)
    {
        scroll = context.ReadValue<Vector2>();
    }

    private void Awake()
    {
        estacion1 = GetComponent<AudioSource>();
        estacion2 = GetComponent<AudioSource>();
        estacion3 = GetComponent<AudioSource>();
    }
    private void Start()
    {
        icon = GameObject.Find("RadioEncendida").GetComponent<Image>();
    }

    void Update()
    {

        EncenderRadio();


        if (scroll.y > 0.0f)
        {
            if (indice <5)
            {
                indice += 1;
            }

            CambiarEstacion();


        }

        if (scroll.y < 0.0f)
        {
            if (indice > 1)
            {
                indice -= 1;
            }

            CambiarEstacion();
        }
    }

    void EncenderRadio()
    {
        if (off == false)
        {
            audioText.gameObject.SetActive(false);
            emisoraText.gameObject.SetActive(false);
            icon.color = Color.red;
            estacion1.mute = true;
            if (radioOn)
            {
                off = true;
            }
        }
        else
        {
            audioText.gameObject.SetActive(true);
            emisoraText.gameObject.SetActive(true);
            icon.color = Color.green;
            estacion1.mute = false;
            if (radioOn)
            {
                off = false;
            }
        }


        
    }
    void CambiarEstacion()
    {
        if(off==true)
        {
            if (indice == 1)
            {
                audioText.text = "Nomada";
                emisoraText.text = "Desert Station";
                estacion1.clip = cancion1;
                estacion1.Play();

            }

            if (indice == 2)
            {

                audioText.text = "Chalobeats Berseker";
                emisoraText.text = "Cyber Dead";
                estacion1.clip = cancion2;
                estacion1.Play();


            }

            if (indice == 3)
            {

                audioText.text = "Prototype";
                emisoraText.text = "IMMORTAL FM";
                estacion1.clip = cancion3;
                estacion1.Play();


            }

            if (indice == 4)
            {

                audioText.text = "Nerso & Verse Enzo";
                emisoraText.text = "FMS 2088 Station";
                estacion1.clip = cancion4;
                estacion1.Play();

            }


            if (indice == 5)
            {

                audioText.text = "Stressed";
                emisoraText.text = "FMS 2088 Station";
                estacion1.clip = cancion5;
                estacion1.Play();

            }
        }
    }
}
