using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreColisionSP : MonoBehaviour
{
    BoxCollider iconP;
    BoxCollider carColPos1;

    void Start()
    {
        iconP = GetComponent<BoxCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        carColPos1 = GameObject.Find("ColPosition").GetComponent<BoxCollider>();
        
        Physics.IgnoreCollision(iconP, carColPos1, true);
       
    }
}
