using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BullEffectMove : MonoBehaviour
{
    public float velocidad;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0, velocidad*Time.deltaTime);
        Destroy(gameObject, 2f);
    }
}
