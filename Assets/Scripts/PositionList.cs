using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PositionList : MonoBehaviour
{
    public Image p1;
    public Image p2;
    public Sprite[] cletus;
    public Sprite[] mhira;
    public Sprite[] zhafiro;
    public SystemPosition controlador;
    public float contCE;
    bool conEnd;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Cursor.lockState = CursorLockMode.Confined;
        Cursor.visible = true;
        
        if(controlador.delorean)
        {
            p1.sprite = mhira[0];
            p2.sprite = zhafiro[1];
            contCE += 0.01f * Time.deltaTime;
        }

        if (controlador.delorean2)
        {
            p1.sprite = mhira[0];
            p2.sprite = cletus[1];
            contCE += 0.01f * Time.deltaTime;
        }


        if (controlador.match)
        {
            p1.sprite = zhafiro[0];
            p2.sprite = mhira[1];
            contCE += 0.01f * Time.deltaTime;
        }

        if (controlador.match2)
        {
            p1.sprite = zhafiro[0];
            p2.sprite = cletus[1];
            contCE += 0.01f * Time.deltaTime;
        }



        if (controlador.batMovil)
        {
            p1.sprite = cletus[0];
            p2.sprite = mhira[1];
            contCE += 0.01f * Time.deltaTime;
        }

        if (controlador.batMovil2)
        {
            p1.sprite = cletus[0];
            p2.sprite = zhafiro[1];
            contCE += 0.01f * Time.deltaTime;
        }

        //if (contCE>0.1)
        //{
        //    SceneManager.LoadScene(4);
        //    conEnd = true;
        //}

        //if(conEnd)
        //{
        //    contCE = 0.0f;
        //}
    }
}
