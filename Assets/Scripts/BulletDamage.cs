using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class BulletDamage : MonoBehaviour
{
    public VisualEffect impact;
    public GameObject blood;
    public float velocidad;
    
    void Update()
    {
        transform.Translate(Vector3.forward * velocidad * Time.deltaTime);
    }


 

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Instantiate(impact, transform.position, transform.rotation);
            Destroy(gameObject);
        }

        if (other.gameObject.CompareTag("Zombie"))
        {
            Instantiate(blood, transform.position, transform.rotation);
            Destroy(other.gameObject);
        }

    }
}
