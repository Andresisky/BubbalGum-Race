using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveLaser : MonoBehaviour
{
    [SerializeField]
    float velocidad = 10.0f;
    public GameObject bloodParticles;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        transform.Translate(0, 0, velocidad * Time.deltaTime);
        Destroy(gameObject, 3f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "CarEnemy")
        {
            Destroy(gameObject);
        }

        if (other.gameObject.tag == "Zombie")
        {
            Instantiate(bloodParticles, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}
