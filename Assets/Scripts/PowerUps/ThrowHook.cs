
using UnityEngine;
using UnityEngine.UI;

public class ThrowHook : MonoBehaviour
{
    
    public GameObject hookOBJ;
    public Transform pivot;
   
    public float speed = 10.0f;
    public PowerUpSystem UpSys;
    
    private Vector3 rotHook;

    //Camera
    public float lookSpeed = 3;
   

    
    void Start()
    {
       
    }

   
    void Update()
    {
        rotHook = new Vector3(hookOBJ.transform.rotation.x, transform.rotation.y, hookOBJ.transform.rotation.z);
       
        if(UpSys.hookActivo)
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                Hook();
                UpSys.habilitarP = false;
                UpSys.hookActivo = false;
            }
            
        }
        
    }

    void Hook()
    {
        var hook = Instantiate(hookOBJ,pivot.transform.position, pivot.rotation);
        hook.GetComponent<HookScript>().caster = transform;
    }
}
