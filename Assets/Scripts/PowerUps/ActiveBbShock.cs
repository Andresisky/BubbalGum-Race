using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveBbShock : MonoBehaviour
{
    public Transform ThrowPoint;
    public GameObject burbuja;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            disparoBubble();
        }
    }

    void disparoBubble()
    {
        Instantiate(burbuja, ThrowPoint.transform.position, ThrowPoint.transform.rotation);
    }
}
