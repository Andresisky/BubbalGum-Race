using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpRandomizer : MonoBehaviour
{
    //NUMEROS DE POWER UP:      0 = LASER       1 = TURBO       2 = HOOK        3 = BUBBLESHOCK         4 = TELEPORT


    public int powerUpNumber = -1;

    public int GivePowerUp()
    {
        if (powerUpNumber < 0)
        {
            powerUpNumber = Random.Range(0, 5);
        }
        return powerUpNumber;
    }

    private void OnTriggerEnter(Collider other)
    {
        GivePowerUp();
        Destroy(gameObject);
    }

}
