using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turbo : MonoBehaviour
{

    public PowerUpSystem upSys;
    public ArcadeCar carControl;
    [SerializeField]
    float constDeAumentoVelocidad;

    public ParticleSystem turbo;

    [SerializeField]
    float timeCont =0.0f;


    bool activo;

    AnimationCurve aux;
    public float turboLimit;

    void Start()
    {
        upSys = GetComponent<PowerUpSystem>();
        aux = carControl.accelerationCurve;
    }

    // Update is called once per frame
    void Update()
    {
        if(upSys.turboActivo)
        {
            Movimiento();
            ActiveTurbo();
           
        }
        
        
        
    }


    public void Movimiento()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            SoundSystem.instance.PlayTurbo();
            turbo.Play();
            activo = true;
            carControl.accelerationCurve = AnimationCurve.Linear(0.0f, 0.0f, 0.09f, turboLimit);
            //carControl.accelerationCurve = aux;
            
        }
    }

    public void ActiveTurbo()
    {
        if(activo)
        {
            timeCont += 0.1f * Time.deltaTime;
        }
        else
        {
            //carControl.accelerationCurve = AnimationCurve.Linear(0.0f, 0.0f, 5.0f, 100.0f);
        }

        if(timeCont > 0.2f)
        {
            activo = false;
            turbo.Stop();
            timeCont = 0.0f;
            carControl.accelerationCurve = aux;
            upSys.habilitarP = false;
            upSys.turboActivo = false;
        }
        
    }

   
}
