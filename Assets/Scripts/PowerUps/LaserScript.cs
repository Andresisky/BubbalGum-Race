using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    public PowerUpSystem UpSys;
    
    public Transform pivot;
    public ParticleSystem laser;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Shooting();
    }

    public void Shooting()
    {


        if (UpSys.laserActivo)
        {
            if (Input.GetKeyDown(KeyCode.V))
            {
                SoundSystem.instance.PlayCannon();
                Instantiate(laser, pivot.position, transform.rotation);
                //PhotonNetwork.Instantiate("Laser", pivot.position, transform.rotation);
                UpSys.habilitarP = false;
                UpSys.laserActivo = false;

            }
        }



    }
}
