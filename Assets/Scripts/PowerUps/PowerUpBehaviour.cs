using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;


public class PowerUpBehaviour : MonoBehaviour
{

    //NUMEROS DE POWER UP:      0 = LASER       1 = TURBO       2 = HOOK        3 = BUBBLESHOCK         4 = TELEPORT
    private int powerUpNumber;
    
    public Transform pivotP;
    public Image barra;
    public float vida;
    bool ballShock;
    public GameObject portal;
    public bool extra;
    //public GameObject effect;
    ArcadeCar carControl;
    PlayerControl controlS;
    public Animator iconImage;
   
    public GameObject laserPointer;
    [SerializeField]private bool powerUpON;

    [Header("Laser Attributes")]
    [SerializeField] bool laser;
    public Transform laserSpawnPoint;
    public GameObject tempLaser;


    [Header("Turbo Attributes")]
    [SerializeField] bool turbo;
    bool turboActivated;
    public ParticleSystem turboParticles;
    AnimationCurve aux;
    float turboLimit = 195f;
    float turboTime = 0.0f;

    [Header("Hook Attributes")]
    [SerializeField] bool hook;
    public Transform hookSpawnPoint;
    public GameObject hookPrefab;


    [Header("Gumball Attributes")]
    [SerializeField] bool gumBall;
    public Transform bubbleSpawnPoint;
    public GameObject bubblePrefab;

    [Header("Teleport Attributes")]
    [SerializeField] bool teleport;


    public bool life;

    private bool powerUp = false;
    public void PowerUp(InputAction.CallbackContext context)
    {
        powerUp = context.action.triggered;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    private void Awake()
    {
       
        laserPointer.SetActive(false);
        carControl = GetComponent<ArcadeCar>();
        aux = carControl.accelerationCurve;
    }
   

    private void Update()
    {
        //vida = Mathf.Clamp(vida, 0, 100);
        //barra.fillAmount = vida / 100;
        controlS = GetComponent<PlayerControl>();
        vida = controlS.hp;
        

        
        TurboTimeCounter();
        if (powerUp && powerUpON == true)
        {
            if (laser == true)
            {
                
                ShootLaser();
                laser = false;
                laserPointer.SetActive(false);
                iconImage.SetBool("Lase", false);
            }
            if (turbo == true)
            {
                
                Turbo();
                turbo = false;
                iconImage.SetBool("Nitro", false);
            }
            if (hook == true)
            {
                
                Hook();
                hook = false;
                laserPointer.SetActive(false);
                iconImage.SetBool("Chain", false);
            }
            if (gumBall == true)
            {
                
                
                
                ShootBubble();
                gumBall = false;
                iconImage.SetBool("Shock", false);
            }
            if (teleport == true)
            {
                Portal();
               
                teleport = false;
                iconImage.SetBool("Port", false);
            }

            if (life == true )
            {

            }
            powerUpON = false;
        }
        
    }


    //////////////////////////// LASER \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    
    public void ShootLaser()
    {
        SoundSystem.instance.PlayCannon();
        Instantiate(tempLaser, laserSpawnPoint.position, laserSpawnPoint.rotation);
    }

    public void Portal()
    {
        if(teleport)
        {
            Instantiate(portal, pivotP.position, pivotP.rotation);
        }
        
    }

    


    //////////////////////////// TURBO \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

                //El metodo TurboTimeCounter() debe estar llamado en el update

    public void Turbo()
    {
        turboActivated = true;
        SoundSystem.instance.PlayTurbo();
        turboParticles.Play();
        carControl.accelerationCurve = AnimationCurve.Linear(0.0f, 0.0f, 0.09f, turboLimit);
    }


    void TurboTimeCounter()
    {
        if (turboActivated)
        {
            turboTime += 0.1f * Time.deltaTime;
        }

        if (turboTime > 0.2f)
        {
            turboActivated = false;
            turboParticles.Stop();
            carControl.accelerationCurve = aux;
            turboTime = 0.0f;
            turbo = false;
        }
    }

    //////////////////////////// HOOK \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\


    public void Hook()
    {
        SoundSystem.instance.PlayHookShot();
        var hook = Instantiate(hookPrefab, hookSpawnPoint.transform.position, hookSpawnPoint.rotation);
        hook.GetComponent<HookScript>().caster = transform;
    }

    //////////////////////////// SHOCKBALL \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    public void ShootBubble()
    {
       
        if(gumBall)
        {
            Instantiate(bubblePrefab, bubbleSpawnPoint.transform.position, bubbleSpawnPoint.transform.rotation);
            
        }
        
    }

   


    void GetPowerUp(int number)
    {
        if (powerUpON == false)
        { 
        
            if(vida>=100)
            {
                if (number == 0)
                {
                    iconImage.SetBool("Lase", true);
                    laser = true;
                    laserPointer.SetActive(true);
                }
                if (number == 1)
                {
                    iconImage.SetBool("Nitro", true);
                    turbo = true;
                }
                if (number == 2)
                {
                    iconImage.SetBool("Chain", true);
                    hook = true;
                    laserPointer.SetActive(true);
                }
                if (number == 3)
                {
                    iconImage.SetBool("Shock", true);
                    gumBall = true;
                }
                if (number == 4)
                {
                    iconImage.SetBool("Port", true);
                    teleport = true;
                }
            }
            



            //if (vida < 100)
            //{
            //    number = 5;
                
            //}
            //if(number==5)
            //{
            //    iconImage.SetBool("Life", true);
            //    life = true;
            //}

        
            powerUpON = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "PowerUp" && powerUpON == false)
        {
            powerUpNumber = Random.Range(3, 5);
            GetPowerUp(powerUpNumber);
        }

        if(other.gameObject.CompareTag("portal"))
        {
            Destroy(other.gameObject);
            transform.position = pivotP.position;
        }
    }


}
