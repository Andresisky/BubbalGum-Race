using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bubble : MonoBehaviour
{
    public bool activo;
    public GameObject electricity;
    BoxCollider bubb;
    BoxCollider carColPos1;
    BoxCollider carColPos2;
    void Start()
    {
        bubb = GetComponent<BoxCollider>();
        carColPos1 = GameObject.Find("ColPosition").GetComponent<BoxCollider>();
        carColPos2 = GameObject.Find("ColP").GetComponent<BoxCollider>();
        Physics.IgnoreCollision(bubb, carColPos1, true);
        Physics.IgnoreCollision(bubb, carColPos2, true);
    }

    // Update is called once per frame
    void Update()
    {
        if (activo == false)
        {
            StartCoroutine("DestroyUpdate");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            activo = true;
            StartCoroutine("Destroy");
            electricity.SetActive(true);
        }
       
    }

    IEnumerator Destroy()
    {
        if(activo)
        {
            yield return new WaitForSeconds(1f);
            Destroy(this.gameObject);
            activo = false;
        }
        yield return new WaitForSeconds(2f);
        Destroy(this.gameObject);
    }

    IEnumerator DestroyUpdate()
    {
        yield return new WaitForSeconds(9f);
        Destroy(this.gameObject);
    }
}
