using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerUpSystem : MonoBehaviour
{
    [SerializeField]
    Image contenedor;
    public Sprite hook;
    public Sprite teleport;
    public Sprite laser;
    public Sprite turbo;
    bool alSelection;
    bool selection;
    public float cont;
    public float contS;
    public int icont;
    public bool hookActivo;
    public bool teleportActivo;
    public bool laserActivo;
    public bool turboActivo;
    public bool habilitarP;
    int seleccionador;

    void Start()
    {
        contenedor = GameObject.Find("PowerUpImage").GetComponent<Image>();
        
    }

    
    void Update()
    {
        AleatorioSelect();
        Habilitar();
        
    }


    void AleatorioSelect()
    {
        

        if (alSelection)
        {
            seleccionador = Random.Range(0, 20) ;
            
            //cont += 0.01f * Time.deltaTime;
            if (seleccionador == 1)
            {
                contenedor.sprite = hook;
            }

            if (seleccionador==3)
            {
                contenedor.sprite = teleport;
                //cont = 0.0f;
            }

            if (seleccionador == 6)
            {
                contenedor.sprite = laser;
                //cont = 0.0f;
            }

            if (seleccionador == 9)
            {
                contenedor.sprite = turbo;
                //cont = 0.0f;
            }
        }

        if (selection)
        {
            contS += 0.1f * Time.deltaTime;
            if( contS > 0.5f)
            {
                alSelection = false;
                selection = false;
                habilitarP = true;
               
                //cont = 0.0f;
                contS = 0.0f;
            }
        }
        

        
    }

    void Habilitar()
    {
        if(habilitarP)
        {
            if (contenedor.sprite == hook)
            {
                hookActivo = true;
            }
            else
            {
                hookActivo = false;
            }

            if (contenedor.sprite == teleport)
            {
                teleportActivo = true;
            }
            else
            {
                teleportActivo = false;
            }

            if (contenedor.sprite ==laser)
            {
                laserActivo = true;
            }
            else
            {
                laserActivo = false;
            }

            if (contenedor.sprite == turbo)
            {
                turboActivo = true;
            }
            else
            {
                turboActivo = false;
            }
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PowerUp"))
        {
            selection = true;
            alSelection = true;
        
            Destroy(other.gameObject);
        }
    }
}
