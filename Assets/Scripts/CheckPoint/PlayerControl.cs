using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
public class PlayerControl : MonoBehaviour
{
    [Header("Needle HUD")]
    public GameObject uiNeedle;
    public GameObject effect;
    bool extra;
    
    [Header("HP System")]
    [SerializeField] Image hpImage;
    
    public float hp = 100f;
    PowerUpBehaviour poweruP;
    [Header("Gas System")]
    [SerializeField] Image gasBarImage;
    [SerializeField] float gasQuantity = 100f;
    float cont = 0.0f;
    public float gasBurned = 0.003f;
    bool charge;
    public bool shock;
    public float timeShock = 0.0f;
    CheckpointSystem controlPoints;
    StartSystemSemaforo semaf;
    public GameObject explosion;
    public GameObject[] wheels;
    public Transform respawn;
   
    CheckpointSystem checkpointSystem;
    private Vector3 playerInitialPosition;
    private float startPosition = 0f, endPosition = -210f;
    private float desiredPosition;
    private float vehicleSpeed;
    private Rigidbody rb;

    [SerializeField] private ArcadeCar carScript;
    private bool speedUp = false;
    private bool speedDown = false;
    private bool resetPos = false;

    public void SpeedUp(InputAction.CallbackContext context)
    {
        speedUp = context.action.triggered;
    }

    public void SpeedDown(InputAction.CallbackContext context)
    {
        speedDown = context.action.triggered;
    }

    public void ResetPos(InputAction.CallbackContext context)
    {
        resetPos = context.action.triggered;
    }

    
    void Start()
    {
        controlPoints = GameObject.Find("Checkpoints").GetComponent<CheckpointSystem>();
        
        
        transform.tag = "Player";
        poweruP = GetComponent<PowerUpBehaviour>();
        playerInitialPosition = transform.position;
        rb = GetComponent<Rigidbody>();
        carScript = GetComponent<ArcadeCar>();
        respawn = GameObject.Find("GasStation").GetComponent<Transform>();
        semaf = GameObject.Find("TraffictLighRace(Clone)").GetComponent<StartSystemSemaforo>();
        checkpointSystem = FindObjectOfType<CheckpointSystem>();
        // rigidbody = GetComponent<Rigidbody>();
        
    }

    void Update()
    {
       
        vehicleSpeed = carScript.GetSpeed();
        gasQuantity = Mathf.Clamp(gasQuantity, 0, 100);
        gasBarImage.fillAmount = gasQuantity / 100;
        hp = Mathf.Clamp(hp, 0, 100);
        hpImage.fillAmount = hp / 100;

        AddGas();
        TakeGas();
        DestroyCar();
        UpdateNeedle();
        
        if (resetPos)
        {
            Reset();
        }

        if (poweruP.life)
        {
            
            if (Input.GetKeyDown(KeyCode.V))
            {
                extra = true;
                
            }
        }
        ExtraLife();
        


    }

    public void ExtraLife()
    {
        if(extra)
        {
            effect.SetActive(true);
            hp += 0.1f;


            
        }

        if (hp >= 100f)
        {
            
            effect.SetActive(false);

            poweruP.life = false;
            extra = false;
            poweruP.iconImage.SetBool("Life", false);
        }
        

    }
    public void SetPosition(Vector3 p)
    {
        transform.position = p;
    }
   

    public void ResetPlayer()
    {
        if (checkpointSystem.CheckpointAvailable())
        {
            transform.position = checkpointSystem.GetCurrentCheckpointPosition();
        }
        else
        {
            transform.position = playerInitialPosition;
        }
    }

    private void UpdateNeedle()
    {
        float temp = vehicleSpeed / 50;
        desiredPosition = startPosition - endPosition;
        if (speedDown)
        {
            endPosition = -210;
        }
        else
        {
            endPosition = 210;
        }

        uiNeedle.transform.eulerAngles = new Vector3(0, 0, (startPosition - temp * desiredPosition));

        if (carScript.GetSpeed() < 0)
        {
            uiNeedle.transform.eulerAngles = new Vector3(0, 0, 0);
        }
    }

    void Reset()
    {
        /*position += new Vector3(UnityEngine.Random.Range(-1.0f, 1.0f), 0.0f, UnityEngine.Random.Range(-1.0f, 1.0f));
        float yaw = transform.eulerAngles.y + UnityEngine.Random.Range(-10.0f, 10.0f);

        transform.position = position;
        transform.rotation = Quaternion.Euler(new Vector3(0.0f, yaw, 0.0f));

        rb.velocity = new Vector3(0f, 0f, 0f);
        rb.angularVelocity = new Vector3(0f, 0f, 0f);

        for (int axleIndex = 0; axleIndex < axles.Length; axleIndex++)
        {
            axles[axleIndex].steerAngle = 0.0f;
        }

        Debug.Log(string.Format("Reset {0}, {1}, {2}, Rot {3}", position.x, position.y, position.z, yaw));*/
        ResetPlayer();
        rb.velocity = new Vector3(0f, 0f, 0f);
        rb.angularVelocity = new Vector3(0f, 0f, 0f);
    }


    void AddGas()
    {
        if (gasQuantity < 100f)
        {
            if (charge)
            {
                cont += 0.1f * Time.deltaTime;
                gasQuantity += 1f;
            }

            if (cont > 0.0015f)
            {

                cont = 0.0f;
                charge = false;
            }
        }
    }

    void TakeGas()
    {
        if (gasQuantity > 0.0f)
        {

            if (speedUp)
            {
                gasQuantity -= gasBurned;
            }

        }
        else
        {
            carScript.controllable = false;
        }
    }

    void DestroyCar()
    {
        if (hp == 0)
        {
            hp = 100;
            Instantiate(explosion, transform.position, Quaternion.identity);
            for (int i = 0; i < wheels.Length; i++)
            {
                Instantiate(wheels[i], transform.position, Quaternion.identity);
            }
            transform.position = respawn.position;


         


        }


    }

    


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("GasCan"))
        {
            charge = true;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("Bullet"))
        {

            hp -= 1.0f;
            Debug.Log("Shot");
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("PowerUp"))
        {
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("Laser"))
        {
            hp -= 10.0f;
            Destroy(other.gameObject);
        }
        if (other.gameObject.CompareTag("limite"))
        {
            ResetPlayer();
        }
       

    }

}
