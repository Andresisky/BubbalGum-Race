using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointSystem : MonoBehaviour
{
    public CheckPoint[] checkpoints;
    public PlayerControl carPlayerControl;
    public int currentCheckpointIndex = 0;

    void Start()
    {
        currentCheckpointIndex = -1;
        GetCheckpointsReferences();
        LoadData();
        carPlayerControl = FindObjectOfType<PlayerControl>();

    }

    void GetCheckpointsReferences()
    {
        int checkpointCount = transform.childCount;
        checkpoints = new CheckPoint[checkpointCount];

        for (int i = 0; i < checkpointCount; i++)
        {
            checkpoints[i] = transform.GetChild(i).GetComponent<CheckPoint>();
            checkpoints[i].SetIndex(i);
            checkpoints[i].SetCheckpointSystem(this);
        }

    }

    void SaveData()
    {
        PlayerPrefs.SetInt(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name, currentCheckpointIndex);
    }

    void LoadData()
    {
        currentCheckpointIndex = PlayerPrefs.GetInt(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name, -1);
        if (currentCheckpointIndex > -1)
        {
            for (int i = 0; i < currentCheckpointIndex; i++)
            {
                checkpoints[i].Activate();
            }

            //PlayerControl carPlayerControl = FindObjectOfType<PlayerControl>();
            carPlayerControl.SetPosition(checkpoints[currentCheckpointIndex].GetCheckpointPosition());
        }
    }

    public void DeleteData()
    {
        PlayerPrefs.DeleteKey(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }


    public bool CheckpointAvailable()
    {
        return currentCheckpointIndex > -1;
    }

    public Vector3 GetCurrentCheckpointPosition()
    {
        return checkpoints[currentCheckpointIndex].GetCheckpointPosition();
    }

    public void CheckpointActivated(int i)
    {
        if (i > currentCheckpointIndex)
        {
            currentCheckpointIndex = i;
            SaveData();
        }
    }
}
