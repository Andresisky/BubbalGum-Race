using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasScript : MonoBehaviour
{
    float velocidad = 100;
    Rigidbody rb;
    Collider col;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        StartCoroutine(SpawnGas());
    }

    
    void Update()
    {
        transform.Rotate(Vector3.up * velocidad * Time.deltaTime);
    }

    IEnumerator SpawnGas()
    {
        yield return new WaitForSeconds(3f);
        rb.isKinematic = true;
        col.isTrigger = true;
    }
}
