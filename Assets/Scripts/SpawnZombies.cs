using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnZombies : MonoBehaviour
{
    public GameObject spawner;


    private void Start()
    {
        spawner.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        spawner.SetActive(true);
    }
}
