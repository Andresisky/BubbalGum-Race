using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SeleccionPLayers : MonoBehaviour
{
    public GameObject arrows1;
    public GameObject arrows2;
    public GameObject[] playerCar1;
    public GameObject[] playerCar2;
    public Transform targetP1;
    public Transform targetP2;
    public Transform targetP3;
    public GameObject powerUp;
    public GameObject gas;
    public GameObject[] c1;
    public GameObject[] c2;
    public GameObject cars1;
    public GameObject cars2;
    public GameObject trafficLights;
    public Image p1Ready;
    public Image p2Ready;
    public Image p1;
    public Image p2;
    public Sprite p3;
    public GameObject posiciones;
    public bool seleccionLista1;
    public bool seleccionLista2;
    public int indiceDeSeleccion=0;
    public int indiceDeSeleccion2 = 0;
    bool primerJugador;
    bool segundoJugador;
    public bool del2;
    public bool red1;
    void Start()
    {
        PlayerPrefs.DeleteAll();
        primerJugador = false;
        segundoJugador = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            primerJugador = true;
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button9))
        {
            
            segundoJugador = true;
        }

        //if(primerJugador && segundoJugador)
        //{
        //    SceneManager.LoadScene(0);

        //}


        if (primerJugador)
        {
            p1.sprite = p3;
            arrows1.gameObject.SetActive(true);
            cars1.SetActive(true);
            
            if (Input.GetKeyDown(KeyCode.Q))
            {
                p1Ready.gameObject.SetActive(true);
                seleccionLista1 = true;
            }

        }

        if (segundoJugador)
        {
            p2.sprite = p3;
            arrows2.gameObject.SetActive(true);
            cars2.SetActive(true);
            
            if(seleccionLista2==false)
            {
                if (Input.GetKeyDown(KeyCode.Joystick1Button5))
                {
                    if (indiceDeSeleccion2 < c1.Length-1 )
                    {
                        indiceDeSeleccion2 += 1;

                    }
                }
                if (Input.GetKeyDown(KeyCode.Joystick1Button4))
                {
                    if (indiceDeSeleccion2 > -1)
                    {

                        indiceDeSeleccion2 -= 1;
                       

                    }
                }
            }
            

            if (Input.GetKeyDown(KeyCode.Joystick1Button1))
            {
                p2Ready.gameObject.SetActive(true);
                seleccionLista2 = true;

            }

        }

        if (indiceDeSeleccion == 0)
        {
            c1[0].gameObject.SetActive(true);
            c1[1].gameObject.SetActive(false);
            c1[2].gameObject.SetActive(false);
        }
        if (indiceDeSeleccion == 1)
        {
            red1 = true;
            c1[0].gameObject.SetActive(false);
            c1[1].gameObject.SetActive(true);
            c1[2].gameObject.SetActive(false);
        }
        if (indiceDeSeleccion == 2)
        {
            c1[0].gameObject.SetActive(false);
            c1[1].gameObject.SetActive(false);
            c1[2].gameObject.SetActive(true);
        }

        if (indiceDeSeleccion2 == 0)
        {
            del2 = true;
            c2[0].gameObject.SetActive(true);
            c2[1].gameObject.SetActive(false);
            c2[2].gameObject.SetActive(false);
        }
        if (indiceDeSeleccion2 == 1)
        {
            c2[0].gameObject.SetActive(false);
            c2[1].gameObject.SetActive(true);
            c2[2].gameObject.SetActive(false);
        }
        if (indiceDeSeleccion2 == 2)
        {
            c2[0].gameObject.SetActive(false);
            c2[1].gameObject.SetActive(false);
            c2[2].gameObject.SetActive(true);
        }

        if(seleccionLista1 && seleccionLista2)
        {
            powerUp.SetActive(true);
            gas.SetActive(true);
            posiciones.SetActive(true);
            Destroy(this.gameObject);
            Instantiate(playerCar1[indiceDeSeleccion], targetP1.position,targetP1.rotation);
            Instantiate(playerCar2[indiceDeSeleccion2], targetP2.position, targetP2.rotation);
            Instantiate(trafficLights, targetP3.position, targetP3.rotation);
            
        }
    }



    public void SelectComplete()
    {
        if(seleccionLista1 && seleccionLista2)
        {
            SceneManager.LoadScene(2);
        }
        
    }


    public void SelectCar()
    {
        if(seleccionLista1==false)
        {
            if (indiceDeSeleccion < c1.Length - 1)
            {
                indiceDeSeleccion += 1;
               
            }
        }
        

       

    }

    public void SelectCarNegativo()
    {
        if(seleccionLista1==false)
        {
            if (indiceDeSeleccion > -1)
            {
                indiceDeSeleccion -= 1;

            }
        }
        

        
    }


   



}
