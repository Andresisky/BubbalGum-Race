using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GasBarSystem : MonoBehaviour
{
    [SerializeField]
    Image barra;

    [SerializeField]
    GameObject car;


    [SerializeField]
    float cantidad = 100f;

    bool aumento;
    bool disminuyendo;
    public float cont = 0.0f;
    
   
    void Start()
    {

    }

   
    void Update()
    {
        cantidad = Mathf.Clamp(cantidad, 0, 100);
        barra.fillAmount = cantidad / 100;

        SumarLitros();
        RestarLitros();

    }

   



    void SumarLitros()
    {
        if(cantidad <100f)
        {
            if (aumento)
            {
                cont += 0.1f * Time.deltaTime;
                cantidad += 1f;
            }

            if (cont > 0.0015f)
            {
                
                cont = 0.0f;
                aumento = false;
            }
        }
    }

    void RestarLitros()
    {
        if (cantidad > 0.0f)
        {

            if (Input.GetKey(KeyCode.W))
            {
                cantidad -= 0.005f;
            }

        }
        else
        {
            car.GetComponent<ArcadeCar>().enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("GasonileCan"))
        {
            aumento = true;
            Destroy(other.gameObject);
        }
    }
}
