using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngineSound : MonoBehaviour
{
    AudioSource audioSource;

    public float minPitch = 0.5f;
    public float maxPitch = 1.5f;

    public ArcadeCar car;
    private float acceleration;

    // Start is called before the first frame update
    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.pitch = minPitch;
    }

    // Update is called once per frame
    void Update()
    {
        acceleration = car.GetSpeed() * 0.05f;
        

        if (acceleration < minPitch)
        {
            audioSource.pitch = minPitch;
        }
        else if (acceleration > maxPitch)
        {
            audioSource.pitch = maxPitch;
        }
        else
        {
            audioSource.pitch = acceleration;
        }

    }
}
