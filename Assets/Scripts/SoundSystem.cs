using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSystem : MonoBehaviour
{
    public static SoundSystem instance;

    public AudioClip audioSpeedUp;
    public AudioClip audioBrake;
    public AudioClip audioMachineGun;
    public AudioClip audioCannon;
    public AudioClip audioTurbo;
    public AudioClip hookShot;
    public AudioClip audioTeleport;
    public AudioClip audioExplosion;

    //dialogos
    //DELOREAN
    public AudioClip seleD;
    public AudioClip seleD2;
    public AudioClip derrotaD;
    public AudioClip derrotaD2;
    public AudioClip victoriaD;
    public AudioClip victoriaD2;
    //MACH5
    public AudioClip seleM;
    public AudioClip seleM2;
    public AudioClip derrotaM;
    public AudioClip derrotaM2;
    public AudioClip victoriaM;
    public AudioClip victoriaM2;
    //BATI
    public AudioClip seleB;
    public AudioClip seleB2;
    public AudioClip derrotaB;
    public AudioClip derrotaB2;
    public AudioClip victoriaB;
    public AudioClip victoriaB2;




    //para ��adir mas fuentes de sonido
    public AudioSource audioSource;
    public AudioSource audioSource2;

    //no se toca put* el que toca
    private void Awake()
    {
        if (SoundSystem.instance == null)//si no tiene nada asigando se lo asigna 
        {
            SoundSystem.instance = this;

        }
        else if (SoundSystem.instance != this)//si tiene asigando y no es la instancia se destuye
        {
            Destroy(gameObject);

        }
    }
    //Ponemos un metodo con el nombre y el audiosource para anadir
    public void PlaySpeedUp()
    {
        PlayAudioClip(audioSpeedUp);
    }
    public void PlayBrake()
    {
        PlayAudioClip(audioBrake);
    }
    public void PlayShoot()
    {
        audioSource.PlayOneShot(audioMachineGun);
    }
    public void PlayCannon()
    {
        PlayAudioClip2(audioCannon);
    }

    public void PlayTurbo()
    {
        PlayAudioClip2(audioTurbo);
    }

    public void PlayTeleport()
    {
        PlayAudioClip2(audioTeleport);
    }

    public void PlayHookShot()
    {
        audioSource.PlayOneShot(hookShot);
    }

    public void PlayExplosion()
    {
        audioSource.PlayOneShot(audioExplosion);
    }

    ///dialogos
    //dellorean
    public void PlayseleD()
    {
        audioSource.PlayOneShot(seleD);
    }
    public void PlayseleD2()
    {
        audioSource.PlayOneShot(seleD2);
    }

    public void PlayderrotaD()
    {
        PlayAudioClip2(derrotaD);
    }
    public void PlayderrotaD2()
    {
        PlayAudioClip2(derrotaD2);
    }

    public void PlayvictoriaD()
    {
        PlayAudioClip2(victoriaD);
    }
    public void PlayvictoriaD2()
    {
        PlayAudioClip2(victoriaD2);
    }
    //MACH5
    //dellorean
    public void PlayseleM()
    {
        audioSource.PlayOneShot(seleM);
    }
    public void PlayseleM2()
    {
        audioSource.PlayOneShot(seleM2);
    }

    public void PlayderrotaM()
    {
        PlayAudioClip2(derrotaM);
    }
    public void PlayderrotaM2()
    {
        PlayAudioClip2(derrotaM2);
    }

    public void PlayvictoriaM()
    {
        PlayAudioClip2(victoriaM);
    }
    public void PlayvictoriaM2()
    {
        PlayAudioClip2(victoriaM2);
    }
    //BATI
    public void PlayseleB()
    {
        audioSource.PlayOneShot(seleB);
    }
    public void PlayseleB2()
    {
        audioSource.PlayOneShot(seleB2);
    }

    public void PlayderrotaB()
    {
        PlayAudioClip2(derrotaB);
    }
    public void PlayderrotaB2()
    {
        PlayAudioClip2(derrotaB2);
    }

    public void PlayvictoriaB()
    {
        PlayAudioClip2(victoriaB);
    }
    public void PlayvictoriaB2()
    {
        PlayAudioClip2(victoriaB2);
    }
    //AUDIO SOURCE

    private void PlayAudioClip(AudioClip audioClip)
    {
        audioSource.clip = audioClip;
        audioSource.Play();
    }
    private void PlayAudioClip2(AudioClip audioClip2)
    {
        audioSource2.clip = audioClip2;
        audioSource2.Play();
    }


    //esto no se toca es del singlenton
    private void OnDestroy()
    {
        if (SoundSystem.instance == this)
        {
            SoundSystem.instance = null;
        }
    }
}
