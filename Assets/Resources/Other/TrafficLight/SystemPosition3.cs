using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemPosition3 : MonoBehaviour
{

    public GameObject finish;
    public SystemPosition delSystem;
    bool shock;
    public float timeShock = 0.0f;
    ArcadeCar control;
    StartSystemSemaforo star;

    void Start()
    {
        delSystem = GameObject.Find("ControlPositions").GetComponent<SystemPosition>();
        star = GameObject.Find("TraffictLighRace(Clone)").GetComponent<StartSystemSemaforo>();
        control = GetComponent<ArcadeCar>();
        control.controllable = false;
        shock = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (star.arranca&&shock==false)
        {
            control.controllable = true;
        }
        

        Bubble();


    }

    void Bubble()
    {
        if (shock)
        {
            control.controllable = false;
            timeShock += 0.01f * Time.deltaTime;

        }

        if (timeShock > 0.02f)
        {
            
            shock = false;
            timeShock = 0.0f;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Delorean"))
        {
           

            delSystem.delorean2 = false;
            delSystem.batMovil= true;

        }

        

        if (other.gameObject.CompareTag("Mach5"))
        {

           
            delSystem.match2 = false;
            delSystem.batMovil2 =true;




        }


    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Meta"))
        {
            //Instantiate(finish);
            delSystem.finish3= true;
          
            Destroy(this.gameObject);

        }

        if (other.gameObject.CompareTag("Bubble"))
        {

            shock = true;

        }
    }
}
