using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemPosition2 : MonoBehaviour
{
    [SerializeField]
    public bool del;
    public bool mat;
    bool shock;
    public float timeShock = 0.0f;
    public GameObject finish;
    ArcadeCar control;
    StartSystemSemaforo star;
    public SystemPosition delSystem;
    public bool segundoBat;
    

   
  
    void Start()
    {
        delSystem = GameObject.Find("ControlPositions").GetComponent<SystemPosition>();
        star = GameObject.Find("TraffictLighRace(Clone)").GetComponent<StartSystemSemaforo>();
        control = GetComponent<ArcadeCar>();
        control.controllable = false;
        shock = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (star.arranca&& shock==false)
        {
            control.controllable = true;
        }

       

        Bubble();






    }

    void Bubble()
    {
        if (shock)
        {
            control.controllable = false;
            timeShock += 0.01f * Time.deltaTime;

        }

        if (timeShock > 0.02f)
        {
            
            shock = false;
            timeShock = 0.0f;
        }

    }


    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Delorean"))
        {
            
            delSystem.match = true;
            delSystem.delorean = false;
        }



        if (other.gameObject.CompareTag("RedKiller"))
        {
            
            delSystem.match2 = true;
            delSystem.batMovil2 = false;
        }




    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Meta"))
        {
            //Instantiate(finish);
            delSystem.finish2 = true;
            Destroy(this.gameObject);

        }

        if (other.gameObject.CompareTag("Bubble"))
        {

            shock = true;

        }
    }


}
