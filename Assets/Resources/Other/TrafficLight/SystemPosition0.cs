using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemPosition0 : MonoBehaviour
{
    [SerializeField]
    public bool bat=false;
    public bool match=false;
    bool shock;
    public float timeShock=0.0f;
    public GameObject finish;
    ArcadeCar control;
    StartSystemSemaforo star;
    public SystemPosition delSystem;
    
    public bool del;

    
    void Start()
    {
        delSystem = GameObject.Find("ControlPositions").GetComponent<SystemPosition>();
        star = GameObject.Find("TraffictLighRace(Clone)").GetComponent<StartSystemSemaforo>();
        control = GetComponent<ArcadeCar>();
        control.controllable = false;
        shock = false;

    }

    
    void Update()
    {
        if(star.arranca&&shock==false)
        {
            control.controllable = true;
        }
        

        Bubble();

    }



    private void OnTriggerExit(Collider other)
    {
        
        if (other.gameObject.CompareTag("Mach5"))
        {

            delSystem.delorean = true;
            delSystem.match = false;
            
        }

        if (other.gameObject.CompareTag("RedKiller"))
        {

            delSystem.delorean2 = true;
            delSystem.batMovil = false;
        }

       


    }

    void Bubble()
    {
        if (shock)
        {
            control.controllable = false;
            timeShock += 0.01f* Time.deltaTime;

        }

        if(timeShock>0.02f)
        {
            
            shock = false;
            timeShock = 0.0f;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Meta"))
        {
            //Instantiate(finish);
            delSystem.finish1 = true;
            Destroy(this.gameObject);

        }

        if (other.gameObject.CompareTag("Bubble"))
        {

            shock = true;

        }
    }
}
