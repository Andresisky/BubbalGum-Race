using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSystemSemaforo : MonoBehaviour
{
    [SerializeField]
    GameObject[] semaf;
    AudioSource s;
    public AudioClip startS;
    bool starG;
    public float timeStart;
    
    public bool arranca;
    public bool activoCar;
    private void Awake()
    {
        
    }
    void Start()
    {
       
        s = GetComponent<AudioSource>();
        s.Play();
        starG = true;
        arranca = false;
    }

    
    void Update()
    {
        
        StartGame();
        
        if (starG)
        {
            //s.PlayOneShot(startS);
            timeStart += 0.01f * Time.deltaTime;
            
        }

       


       
    }



    void StartGame()
    {
        if(timeStart >0.01f)
        {
            
            semaf[0].SetActive(true);
            
        }
        if (timeStart > 0.02f)
        {
            semaf[1].SetActive(true);
            semaf[0].SetActive(false);
        }

        if (timeStart > 0.03f)
        {
            semaf[2].SetActive(true);
            semaf[1].SetActive(false);
        }

        if (timeStart > 0.04f)
        {
            semaf[3].SetActive(true);
            semaf[2].SetActive(false);
        }

        if (timeStart > 0.05f)
        {
            semaf[4].SetActive(true);
            semaf[3].SetActive(false);

        }

        if (timeStart > 0.06f)
        {
            semaf[5].SetActive(true);
            semaf[4].SetActive(false);
        }

        if (timeStart > 0.07f)
        {
            semaf[6].SetActive(true);
            semaf[5].SetActive(false);
            
            
        }


        if(timeStart>0.075)
        {
            arranca = true;
            for(int i=0;i<semaf.Length;i++)
            {
                semaf[i].SetActive(false);
            }
            
            starG = false;
            timeStart = 0.0f;
            
            

        }

        

    }
}
