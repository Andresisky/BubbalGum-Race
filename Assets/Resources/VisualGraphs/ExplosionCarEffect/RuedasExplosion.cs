using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RuedasExplosion : MonoBehaviour
{
    public float fuerzaY;
    public float fuerzaX;
    public float fuerzaZ;
    Rigidbody rb;
    void Start()
    {
        rb = GetComponent<Rigidbody>();
       
    }

    
    void Update()
    {
        rb.AddForce(new Vector3(fuerzaX, fuerzaY, fuerzaZ), ForceMode.Force);
        Destroy(this.gameObject, 1f);
    }
}
